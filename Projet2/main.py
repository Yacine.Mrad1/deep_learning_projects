#%%
import modules as mm
import torch
import matplotlib.pyplot as plt
from importlib import reload
import numpy as np
mm = reload(mm) #reload each time the module to avoid having to restart the kernel
torch.set_grad_enabled(False) #as asked

torch.manual_seed(1234)

radius = 1/torch.sqrt(torch.tensor(torch.pi*2)) #radius of the circle
train_input, train_target = mm.generate_disc_set(1000, one_hot=False, radius=radius) #generate training set
test_input, test_target = mm.generate_disc_set(1000, one_hot=False, radius=radius) #generate testing set
train_mean, train_std = train_input.mean(), train_input.std() #compute mean and std of training set
train_input = (train_input - train_mean)/train_std #normalize training set
test_input = (test_input - train_mean)/train_std #normalize testing set
#train_target, test_target = (train_target.float()*2-1), (test_target.float()*2-1) #converts 0 to -1 and 1 to 1 (only needed for Tanh)

nb_train_samples = train_input.size(0)
learning_rate = 1e-1 / nb_train_samples

torch.manual_seed(1234)
#___ Create model
model = mm.Module(learning_rate=learning_rate, classification_threshold=0.5)
model.Sequential(
    mm.Linear(2, 25, name='Linear1'),
    mm.ReLU(name='ReLU1'),
    mm.Linear(25, 25, name='Linear2'),
    mm.ReLU(name='ReLU2'),
    mm.Linear(25, 25, name='Linear3'),
    mm.ReLU(name='ReLU3'),
    mm.Linear(25, 1, name='Linear4'),
    mm.Sigmoid()
)

model.set_classification_threshold() #automatically set classification threshold to 0.5 or 0.0 depending on the last layer

model.loss = mm.Loss()
losses,losses_validation  = model.train(train_input,
                                        train_target,
                                        10,
                                        validation_input=test_input,
                                        validation_target=test_target)
predicted = model.predict(test_input)
error_rate = model.error_rates
error_rate_valid = model.error_rates_valid
train_input = train_input*train_std+train_mean #unnormalize (for plot)
test_input = test_input*train_std+train_mean #unnormalize (for plot)


#%%
#___ Plotting the results
fh, ax = plt.subplots(1,1)
ax.plot(losses, label='training set')
ax.plot(losses_validation, label='validation set')
ax.set_xlabel('epochs')
ax.set_ylabel('loss')
ax.legend()
fh.show()
fh.savefig('losses.png')


plt.figure(3)
fh, ax = plt.subplots(1,1)
ax.plot(model.error_rates, label='training set')
ax.plot(model.error_rates_valid, label='validation set')
ax.set_xlabel('epochs')
ax.set_ylabel('error-rate')
ax.legend()
fh.show()
fh.savefig('error_rate.png')


plt.figure(2)
fh, (ax1, ax2) = plt.subplots(1,2)
ax1.scatter(test_input[:,0],test_input[:,1],c=predicted) #plot data with color
circle = plt.Circle((0.5, 0.5),radius,  color='r', fill=False)
ax1.add_patch(circle)
ax1.axis('equal')
ax1.set_title('test-set predicted')
ax2.scatter(test_input[:,0],test_input[:,1],c=test_target) #plot data with color
circle = plt.Circle((0.5, 0.5),radius,  color='r', fill=False)
ax2.add_patch(circle)
ax2.axis('equal')
ax2.set_title('test-set target')
plt.show()
fh.savefig('results.png')

