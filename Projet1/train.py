import dlc_practical_prologue as prologue
import torch
from architectures import *
import numpy as np
import matplotlib.pyplot as plt



 
def test(net, _input, target, batch_size, flag_AL):
    error_cnt = 0
    len_input = _input.size(0)
    
    for i in range(0, len_input, batch_size):
        batch = _input.narrow(0, i, batch_size)
        output = net(batch)
        
        if flag_AL:
            res, output = net(batch)
        
        result_binary = []
        for x in range(output.size(0)):
            if (output.view(-1)[x].item()) < 0.5:
                result_binary.append(0.0)
            else:
                result_binary.append(1.0)
                
        batch_true_y = target.narrow(0, i, batch_size)
        
        for batch_i in range(batch_size):
            if batch_true_y[batch_i] != result_binary[batch_i]:
                error_cnt = error_cnt + 1
                
    error = float(error_cnt) / len_input           
    return 1 - error

# [flag, train_classes, (multiplicators)]
def train(net, _input, target, lr, batch_size, epochs, AL_data):
    losses = np.zeros(epochs)
    accuracies = np.zeros(epochs)
    
    multiplicator = 0.01
    decay = 0
    auxiliary_loss_flag = AL_data[0]
    
    if auxiliary_loss_flag:
        loss_type_AL = nn.CrossEntropyLoss()
        train_classes = AL_data[1]
        multiplicator = 0.1
        decay = 0.00001
        mul_first, mul_second, mul_third = AL_data[2] # 0.05 0.05 0.1
    
    for i in range(epochs):
        error_cnt = 0
        epoch_loss = 0.0
        len_input = _input.size(0)
        
        for j in range(0, len_input, batch_size):
            batch_true_y = target.narrow(0, j, batch_size)
            
            batch_out = _input.narrow(0, j, batch_size)
            result = net(batch_out)
        
            if auxiliary_loss_flag:
                output_AL, result = net(batch_out)
                label_class1 = train_classes[:,0].narrow(0, j, batch_size)
                label_class2 = train_classes[:,1].narrow(0, j, batch_size)
                first = loss_type_AL(output_AL[:,0:10], label_class1)
                second = loss_type_AL(output_AL[:,10:20], label_class2)
                third = loss_type_AL(result.view(-1), batch_true_y.float())
                loss = (mul_first * first) + (mul_second * second) + (mul_third * third)
            else:
                loss_type = nn.BCELoss()
                loss = loss_type(result.view(-1), batch_true_y.float())
                
                
            batch_loss = loss.item()
            epoch_loss += batch_loss

            
            optimizer = torch.optim.Adam(net.parameters(), lr, weight_decay=decay)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            with torch.no_grad():
                for param in net.parameters():
                    param = param - (multiplicator * param.grad)
            
            result_binary = []
            for x in range(result.size(0)):
                if (result.view(-1)[x].item()) < 0.5:
                    result_binary.append(0.0)
                else:
                    result_binary.append(1.0)
            
            for batch_i in range(batch_size):
                if batch_true_y[batch_i] != result_binary[batch_i]:
                    error_cnt = error_cnt + 1
                    
        error_epoch = float(error_cnt) / len_input
        losses[i] = epoch_loss
        accuracies[i] = (1 - error_epoch)
        
    return losses, accuracies
            


if __name__ == "__main__":

    
    all_losses_train, all_acc_train, all_acc_test = [], [], []
    ws_all_losses_train, ws_all_acc_train, ws_all_acc_test = [], [], []
    al_ws_all_losses_train, al_ws_all_acc_train, al_ws_all_acc_test = [], [], []
    
    
    for i in range(10):
        alex = AlexNet(200)
        train_input, train_target, train_classes, test_input, test_target, test_classes = prologue.generate_pair_sets(1000)

        print(i)
        lr = 0.001
        losses, accuracies = train(alex, train_input, train_target, lr, 100, 30, [False])
        all_losses_train.append(losses[-1])
        all_acc_train.append(accuracies[-1])
        
        acc_test = test(alex, test_input, test_target, 100, False)
        all_acc_test.append(acc_test)
    
    print("losses train", all_losses_train)
    print("mean ", np.mean(all_losses_train))
    print("std ", np.std(all_losses_train))
    print("\n")
    print("accuracies train", all_acc_train)
    print("mean ", np.mean(all_acc_train))
    print("std ", np.std(all_acc_train))
    print("\n")
    print("test accuracy ", all_acc_test)
    print("mean ", np.mean(all_acc_test))
    print("std ", np.std(all_acc_test))
    
    for i in range(10):
        alex_ws = WeighShare_CNN(300)
        train_input, train_target, train_classes, test_input, test_target, test_classes = prologue.generate_pair_sets(1000)

        print(i)
        lr = 0.001
        losses_ws, accuracies_ws = train(alex_ws, train_input, train_target, lr, 100, 25, [False])
        ws_all_losses_train.append(losses_ws[-1])
        ws_all_acc_train.append(accuracies_ws[-1])
        
        acc_test = test(alex_ws, test_input, test_target, 100, False)
        ws_all_acc_test.append(acc_test)
    
    print("losses train", ws_all_losses_train)
    print("mean ", np.mean(ws_all_losses_train))
    print("std ", np.std(ws_all_losses_train))
    print("\n")
    print("accuracies train", ws_all_acc_train)
    print("mean ", np.mean(ws_all_acc_train))
    print("std ", np.std(ws_all_acc_train))
    print("\n")
    print("test accuracy ", ws_all_acc_test)
    print("mean ", np.mean(ws_all_acc_test))
    print("std ", np.std(ws_all_acc_test))
    
    for i in range(10):
        alex_ws_al = AuxiLoss_WeighShare_CNN(300)
        train_input, train_target, train_classes, test_input, test_target, test_classes = prologue.generate_pair_sets(1000)

        print(i)
        lr = 0.001
        losses_wsal, accuracies_wsal = train(alex_ws_al, train_input, train_target, lr, 100, 30, [True, train_classes, (0.05, 0.05, 0.1)])
        al_ws_all_losses_train.append(losses_wsal[-1])
        al_ws_all_acc_train.append(accuracies_wsal[-1])
        
        acc_test = test(alex_ws_al, test_input, test_target, 100, True)
        al_ws_all_acc_test.append(acc_test)
    
    print("losses train", al_ws_all_losses_train)
    print("mean ", np.mean(al_ws_all_losses_train))
    print("std ", np.std(al_ws_all_losses_train))
    print("\n")
    print("accuracies train", al_ws_all_acc_train)
    print("mean ", np.mean(al_ws_all_acc_train))
    print("std ", np.std(al_ws_all_acc_train))
    print("\n")
    print("test accuracy ", al_ws_all_acc_test)
    print("mean ", np.mean(al_ws_all_acc_test))
    print("std ", np.std(al_ws_all_acc_test))
    
    lr = 0.001
    losses, accuracies = train(alex, train_input, train_target, lr, 100, 30, [False])
    losses_ws, accuracies_ws = train(alex_ws, train_input, train_target, lr, 100, 30, [False])
    losses_wsal, accuracies_wsal = train(alex_ws_al, train_input, train_target, lr, 100, 30, [True, train_classes, (0.05, 0.05, 0.1)])

    
    x = np.arange(30)
    plt.plot(x, accuracies, 'g', label="Alexnet")
    plt.plot(x, accuracies_ws, 'r', label="Alexnet (with WS)")
    plt.plot(x, accuracies_wsal, 'b', label="Alexnet (with WS and AL)")
    plt.xlabel("Epochs")
    plt.ylabel('Accuracy')
    plt.title("Accuracy through epochs for Alexnet for 1 run")
    plt.legend()
    plt.show()
        
    
    # lr = 0.001
    # loss_train_NN, acc_train_NN = train(alex, train_input, train_target, lr, 100, 25, [False])
    
    # print(loss_train_NN)
    # print(acc_train_NN)
    # print("\n\n")
    
    # acc_test_NN = test(alex, test_input, test_target, 100, False)
    
    # print("acc test sans rien : ", acc_test_NN)
    
    # alex_ws = CNN_WS(200)
    
    # lr = 0.001
    # loss_train_NN, acc_train_NN = train(alex_ws, train_input, train_target, lr, 100, 25, [False])
    
    # print(loss_train_NN)
    # print(acc_train_NN)
    # print("\n\n")
    
    # acc_test_NN = test(alex_ws, test_input, test_target, 100, False)
    
    # print("acc test WS : ", acc_test_NN)


    # alex_ws_al = CNN_WS_AL(200)
    
    # lr = 0.001
    # loss_train_NN, acc_train_NN = train(alex_ws_al, train_input, train_target, lr, 100, 50, [True, train_classes, (0.05, 0.05, 0.1)])
    
    
    # print(loss_train_NN)
    # print(acc_train_NN)
    # print("\n\n")
    
    # acc_test_NN = test(alex_ws_al, test_input, test_target, 100, True)
    
    # print("acc test WS AL: ", acc_test_NN)
    
