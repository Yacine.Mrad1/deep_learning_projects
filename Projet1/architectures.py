from os import error
import torch
from torch import nn
from torch.nn import functional as F
from torch.nn.modules.activation import Sigmoid, Softmax


## AlexNet CNN 5 hidden layers
class AlexNet(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.features = nn.Sequential(
            nn.Conv2d(2, 32, kernel_size=3),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=1),
            
            nn.Conv2d(32, 64, kernel_size=3),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),

            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),

            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),

            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size = 2, stride = 2, padding=1),

        )

        self.classifier = nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(1152, 100),
            nn.ReLU(inplace=True),

            nn.Dropout(0.5),
            nn.Linear(100, 100),
            nn.ReLU(inplace = True),

            nn.Linear(100, 1),

            Sigmoid(),
        )

    def forward(self, x):
        x = self.features(x)
        x = x.reshape(x.size(0), -1)
        x = self.classifier(x)
        return x


# CNN AlexNet to classify the images (give labels)
class Classifier_AlexNet(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.features = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=1),
            
            nn.Conv2d(32, 64, kernel_size=3),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),

            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),

            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),

            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size = 2, stride = 2, padding=1),
        )

        self.classifier = nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(1152, 100),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(100),

            nn.Dropout(0.5),
            nn.Linear(100, 100),
            nn.ReLU(inplace = True),
            nn.BatchNorm1d(100),
            
            nn.Linear(100, 10),

            Sigmoid(), # Should logically be a Softmax but a Sigmoid works better 
        )

    def forward(self, x, y):
        x = self.features(x.unsqueeze(1))
        y = self.features(y.unsqueeze(1))
        x = self.classifier(x.view(-1, 1152))
        y = self.classifier( y.view(-1, 1152))
        z = torch.cat((x, y), 1)
        return z


# MLP NN w/ 3 hidden layers used to tell us if greater or smaller
class MLP(nn.Module):
    def __init__(self, hidn_layrs):
        super().__init__()

        self.classifier = nn.Sequential(
             nn.Linear(20, hidn_layrs, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(hidn_layrs),

            nn.Linear(hidn_layrs, hidn_layrs, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(hidn_layrs),

            nn.Linear(hidn_layrs, hidn_layrs, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(hidn_layrs),

            nn.Linear(hidn_layrs, 1, bias=True),
            Sigmoid()
        )

    def forward(self, x):
        x = self.classifier(x)
        return x

# Weight Sharing CNN (labelize then classifier > or <)
class WeighShare_CNN(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.labelize = Classifier_AlexNet(nb_hidden)
        self.compare = MLP(nb_hidden)

    def forward(self, x):
        x1 = self.labelize(x[:, 0], x[:,1])
        x2 = self.compare(x1)
        return x2


# Weights sharing + Auxiliary losse (labelize then classifier > or <)
class AuxiLoss_WeighShare_CNN(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.labelize = Classifier_AlexNet(nb_hidden)
        self.compare = MLP(nb_hidden)

    def forward(self, x):
        x1 = self.labelize(x[:,0], x[:,1])
        x2 = self.compare(x1)
        return x1, x2
